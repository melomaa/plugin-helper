"""@package pluginhelper
Display labels for plugin controls
"""
from pathlib import Path
import argparse
import textwrap
import json
import tkinter as tk
import fileparser

CONFIG_PATH = "~/.config/pluginhelper/"
DEFAULT_CONFIG = CONFIG_PATH + "default.json"
ICON_IMAGE = CONFIG_PATH + "plug.png"
DEFAULT_DATA_FILE = 'plugin_data.json'


def validate_path(in_path):
    """
    Check path and expand user home directory as necessary

    :param in_path: Path to check
    :return: Validated path
    """
    out_path = in_path
    if in_path.startswith('~'):
        home = str(Path.home())
        out_path = home + in_path[1:]
    return out_path


def get_full_path(file, r_path):
    """
    Get full path to a file. Relative paths are prepended with resource path.

    :param file: Initial file path
    :param r_path: Resource path
    :return: Absolute path to file
    """
    out = validate_path(file)
    if not out.startswith('/'):
        out = r_path + out
    return out


class LabelDisplay:
    """
    Class for showing labels on image of controls
    """
    label_pos = [[0, 0]]
    plugin_list = []
    plug_cv = None
    plugin_data = {}
    bg_image = None
    root = None
    dev_config = {}

    def __init__(self, title, config, plugin_file):
        self.info_file = plugin_file
        r_path = self.read_config(config)
        self.create_window(title, r_path)
        self.create_canvas('Select')
        self.device_coordinates()
        self.set_plugin_data()

    def read_config(self, config):
        """
        Read configuration and store information to class variables.

        :param config: Configuration dictionary
        :return: Resource path
        """
        r_path = ''
        if "resource_path" in config:
            r_path = validate_path(config["resource_path"])
            if not r_path.endswith('/'):
                r_path += '/'
        if not self.info_file:
            self.info_file = r_path + config["plugin_data"]
        if "controllers" in config:
            dev_path = get_full_path(config["controllers"], r_path)
            with open(dev_path, 'r') as dev_file:
                data = json.load(dev_file)
                self.dev_config = data[config["device"]]
        return r_path

    def create_window(self, title, r_path):
        """
        Create top level window for UI

        :param title: Window title
        :param r_path: Resource path
        """
        self.root = tk.Tk()
        self.root.title(title)
        image_path = get_full_path(self.dev_config["image_file"], r_path)
        self.bg_image = tk.PhotoImage(file=image_path)
        self.root.geometry("%dx%d+50+872" % (self.bg_image.width(), self.bg_image.height()))
        self.root.resizable(False, False)
        photo = tk.PhotoImage(file=ICON_IMAGE)
        self.root.iconphoto(False, photo)

    def device_coordinates(self):
        """
        Read coordinates for controls
        """
        if "coordinates" in self.dev_config:
            self.label_pos.extend(self.dev_config["coordinates"])
        elif "pos_parts" in self.dev_config:
            for item in self.dev_config["pos_parts"]:
                self.label_pos.extend(self.dev_config[item])
        else:
            print('WARNING: No coordinates found for control labels!!')

    def set_plugin_data(self):
        """
        Assign plugin information to class variables
        """
        with open(self.info_file, 'r') as plugin_file:
            data = json.load(plugin_file)
            self.plugin_data = data
            self.plugin_list = list(data.keys())

    def create_canvas(self, plugin_name):
        """
        Create canvas with background image and select button

        :param plugin_name: Name of the current plugin
        :return: Image reference, canvas reference
        """
        self.plug_cv = tk.Canvas(self.root, width=self.bg_image.width(),
                                 height=self.bg_image.height(), bg="black")
        self.plug_cv.pack(side='top', fill='both', expand='yes')
        self.plug_cv.create_image(0, 0, image=self.bg_image, anchor='nw')
        self.plug_cv.grid_rowconfigure(2, weight=1)
        button = tk.Button(self.plug_cv, text=plugin_name, command=self.open_selection,
                           bg='#0081ad', width=30, highlightbackground='#00addc',
                           activebackground='#40addc')
        button.grid(row=3, column=0)

    def display_labels(self, plugin_name):
        """
        Display labels for plugin controls

        :param plugin_name: Name of the current plugin
        """
        try:
            for key, value in self.plugin_data[plugin_name].items():
                if value.startswith('!'):
                    self.plug_cv.create_text(self.label_pos[int(key)][0],
                                             self.label_pos[int(key)][1],
                                             text=value[1:], fill="#de6954",
                                             anchor="se", angle=90)
                else:
                    self.plug_cv.create_text(self.label_pos[int(key)][0],
                                             self.label_pos[int(key)][1],
                                             text=value, fill="#fab526", anchor="se", angle=90)
        except (ValueError, IndexError):
            pass

    def assign_selection(self, evt):
        """
        Redraw UI according to the selected plugin

        :param evt: Selection event object
        """
        plugin_select = evt.widget
        index = int(plugin_select.curselection()[0])
        value = plugin_select.get(index)
        self.plug_cv.destroy()
        self.plug_cv.update()
        self.create_canvas(value)
        self.display_labels(value)
        select_window = plugin_select.master
        select_window.destroy()
        select_window.update()

    def open_selection(self):
        """
        Open plugin selection view
        """
        select_window = tk.Toplevel()
        select_window.wm_attributes('-type', 'splash')
        select_window.transient(master=self.root)
        var = tk.StringVar(value=self.plugin_list)
        plugin_select = tk.Listbox(master=select_window, listvariable=var, bg='#bfd1e3')
        list_width = plugin_select.cget('width')
        select_width = list_width * 8
        select_height = len(self.plugin_list) * 18
        select_x = self.root.winfo_x()
        select_y = self.root.winfo_y()
        select_window.geometry("%dx%d+%d+%d" % (select_width, select_height, select_x, select_y))
        plugin_select.pack(side='left', fill='y')
        plugin_select.bind("<<ListboxSelect>>", self.assign_selection)

    def run(self):
        """
        Run UI event loop
        """
        self.root.mainloop()


def display_ui(info_file):
    """
    Initialize display class and show UI

    :param info_file: Plugin information file
    """
    try:
        with open(validate_path(DEFAULT_CONFIG), 'r') as config_file:
            local_config = json.load(config_file)
            display = LabelDisplay('Plugin helper', local_config, info_file)
        display.run()
    except FileNotFoundError as file_err:
        print('File not found: %s' % str(file_err))
    except KeyError as key_err:
        print('Configuration missing required field:')
        print(key_err)


def control_usage(info_file):
    """
    Count and display usage stats for plugin control IDs.

    :param info_file: Plugin information file
    """
    toggle_cnt = [0] * 20
    ctrl_cnt = [0] * 20
    plugin = ''
    try:
        with open(info_file, 'r') as plug_file:
            plugin_data = json.load(plug_file)
        plugin_list = list(plugin_data.keys())
        for plugin in plugin_list:
            for key, value in plugin_data[plugin].items():
                if value.startswith('!'):
                    toggle_cnt[int(key)] += 1
                else:
                    ctrl_cnt[int(key)] += 1
        for idx in range(1, 19):
            print('%2d: ctrl: %2d, tog: %2d, total: %2d' % (
                idx, ctrl_cnt[idx], toggle_cnt[idx], (ctrl_cnt[idx] + toggle_cnt[idx])))
    except FileNotFoundError:
        print('Plugin information file not found: %s' % info_file)
    except (ValueError, IndexError):
        print('Counting failed for %s' % plugin)


def init_argparse() -> argparse.ArgumentParser:
    """
    Initialize command line argument parsing
    :return: Argument parser instance
    """
    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(
        usage="%(prog)s [OPTION] [FILE]...",
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''\
            Display for plugin control labels and utility operations.
            Options are related to utilities and are not used when the UI is displayed.
            The FILE argument specifies the file containing the plugin information.
        ''')
    )
    parser.add_argument(
        "-v", "--version", action="version",
        version=f"{parser.prog} version 1.0.0"
    )
    parser.add_argument(
        "-p", "--parse", nargs='?', metavar='TEXT FILE',
        help="Parse text file for plugin information. Output file can be given as last argument."
    )
    parser.add_argument(
        "-u", "--uses", nargs='?', const=DEFAULT_DATA_FILE, metavar='PLUGIN FILE',
        help="Count and display usage stats for controls."
    )
    parser.add_argument('files', nargs='*')
    return parser


def main():
    """
    Main function
    """
    data_file = None
    args = init_argparse().parse_args()
    if args.files:
        data_file = args.files[0]
    if args.parse:
        fileparser.get_plugin_info(args.parse, data_file)
    elif args.uses:
        control_usage(args.uses)
    else:
        display_ui(data_file)


if __name__ == '__main__':
    main()
