"""@package fileparser
Parser plugin information from text file
and print or store in JSON format
Text file should be in following format:

PLUGIN NAME:  (Name of the plugin followed by colon)
CTRL_ID CONTROL LABEL (ID of a control and label to display)
CTRL_ID !TOGGLE LABEL  (ID of a toggle control and label with a leading exclamation mark)

For example:
------------
Ardour Compressor:
1 Attack
2 Release
10 !Enable
"""
import sys
import re
import json

PLUGIN_NAME_REGEX = re.compile('^(\\S+.*):')
PLUGIN_CTRL_REGEX = re.compile('(^[0-9]*)\\s+(\\S.*)')


def parse_file(file):
    """
    Parse plugin information from a text file line by line.

    :param file: Name of the file
    :return: Plugin information dictionary
    """
    plugin_name = ""
    plugin_data = {}
    with open(file, 'r') as plugin_file:
        lines = plugin_file.readlines()
        for line in lines:
            if line.isspace():
                if plugin_name:
                    plugin_name = ""
                continue
            if plugin_name:
                match = PLUGIN_CTRL_REGEX.match(line)
                if match:
                    ctrl_id = match.group(1)
                    ctrl_label = match.group(2)
                    plugin_data[plugin_name][ctrl_id] = ctrl_label
            else:
                match = PLUGIN_NAME_REGEX.match(line)
                if match:
                    plugin_name = match.group(1)
                    plugin_data[plugin_name] = {}
    return plugin_data


def get_plugin_info(text_file, output=None):
    """
    Get plugin information.

    :param text_file: File to read
    :param output: Output file (optional)
    """
    try:
        pl_data = parse_file(text_file)
        if output:
            with open(output, 'w') as out_file:
                json.dump(pl_data, out_file, indent=1)
                print('Printed to %s' % output)
        else:
            print(json.dumps(pl_data, indent=1))
    except FileNotFoundError:
        print('File not found: %s' % text_file)
    except IOError:
        print('Failed to store plugin information.')


if __name__ == '__main__':
    if len(sys.argv) > 1:
        get_plugin_info('Plugin_Ctrl_IDs.txt', sys.argv[1])
    else:
        get_plugin_info('Plugin_Ctrl_IDs.txt', None)
