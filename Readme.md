Plugin Label Helper
===================

Display labels on top of an image of a MIDI controller.

This is used to show which knob/slider controls a specific parameter of a plugin.
